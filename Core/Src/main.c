/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "iwdg.h"
#include "usart.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "internal_flash_test.h"
#include "modbus_test.h"
#include "cli_process.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//for modbus uart port
extern uint8_t rx_chr;
extern uint8_t modbus_rx_buffer[MAX_READ_PER_TRANSACTION];
extern uint8_t current_index;

//for debug uart port
extern volatile uint8_t debug_rx_char;
extern volatile uint8_t debug_rx_buffer[RX_BUFFER_LENGTH];
extern volatile int  debug_rx_buffer_read_pos;
extern volatile int  debug_rx_buffer_write_pos;

extern volatile uint8_t debug_tx_buffer[TX_BUFFER_LENGTH];
extern volatile int  debug_tx_buffer_read_pos;
extern volatile int  debug_tx_buffer_write_pos;

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

static int watchdog_was_reset_source = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

int fputc(int ch, FILE *f)
{
	
  HAL_UART_Transmit(&DEBUG_PORT_HANDLE, (uint8_t *)&ch, 1, 0xFFFF);
  
  return ch;
}

int fgetc(FILE *f)
{
  uint8_t  ch;
	
  HAL_UART_Receive(&DEBUG_PORT_HANDLE,(uint8_t *)&ch, 1, HAL_MAX_DELAY);
	
  return  ch;
} 

/*
 * If add codes below, will do not need to use MicroLIB
*/

#pragma import(__use_no_semihosting)
 
struct __FILE
{
	int a;
};
 
FILE __stdout;
FILE __stdin;
void _sys_exit(int x)
{
	
}

int watchdog_reset_occured(void)
{
	//if(REG_Watchdog_reset->WWDG_RSTF)
	if(__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST))
	{
		return 1;
	}
	return 0;
}

void clear_reset_source(void)
{
	__HAL_RCC_CLEAR_RESET_FLAGS();
	//REG_Watchdog_reset->RMVF = 1;
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_USART4_UART_Init();
  MX_I2C1_Init();
  MX_SPI2_Init();
  MX_LPUART1_UART_Init();
  MX_IWDG_Init();
  /* USER CODE BEGIN 2 */

	if(watchdog_reset_occured())
	{
		watchdog_was_reset_source = 1;
	}
	clear_reset_source();

	Debug_printf("Test project, Thermokon HAL! \r\n");
	//Debug_printf("Debug print test\r\n");
	detect_thermokon_sensor();

	if(!watchdog_was_reset_source)
	{
			//if the reset source was not the watchdog, then we will do the cli.
			main_handle_cli();
	}
	//internal_flash_test();
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	//char string[20] ={0};
	
  while (1)
  {	
		
  		HAL_IWDG_Refresh(&hiwdg);
		
		HAL_GPIO_TogglePin(LED_STATE_GPIO_Port, LED_STATE_Pin);
		HAL_Delay(1000);
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_LPUART1
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{	

//for modbus port
	if(huart->Instance == LPUART1)
	{ 
		
		modbus_rx_buffer[current_index++] = rx_chr;
		HAL_UART_Receive_IT(&hlpuart1, &rx_chr,1); // save char from uart receive
	}
//for debug port
	if(huart->Instance == USART1)
	{ 
		//if we receive a character, assume that the command line is active
		cliActive = 1;
		//we have a character in the receive buffer.
		//we potentially only have 1ms to read this character before the next one arrives

		//debug_rx_buffer[debug_rx_buffer_write_pos] = REG_Debug_RDR->RDR;
		
		debug_rx_buffer[debug_rx_buffer_write_pos] = debug_rx_char;
		//reading the RDR register clears the interrupt flag
		debug_rx_buffer_write_pos++;
		debug_rx_buffer_write_pos = debug_rx_buffer_write_pos % RX_BUFFER_LENGTH;
		
//		HAL_UART_Transmit_IT(&DEBUG_PORT_HANDLE,(uint8_t *)&debug_rx_char, 1);
		HAL_UART_Receive_IT(&DEBUG_PORT_HANDLE,(uint8_t *)&debug_rx_char, 1);
		/*
		if(at_uart_bridge_flag)
		{
        	io_link_AddToWriteBuffer((char *)REG_Debug_RDR, 1);
		} 
		*/		
	}

}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
